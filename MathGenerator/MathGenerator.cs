﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MathGenerator
{
    public class MathGenerator
    {
        public List<String> functions;
        public List<MathOutput> GenerateMath(List<String> functions, int numberOfQuestions)
        {
            bool multiFlag = false;
            Random rnd = new Random();
            List<MathOutput> mathOutputList = new List<MathOutput>();
            for (int i = 0; i < numberOfQuestions; i++)
            {
                
                int index = rnd.Next(functions.Count());
                String function = functions[index].ToString();
                switch (function)
                {
                    case "Add_1":
                        mathOutputList.Add(GenerateAddition(rnd,0, 100,((RadioButton)Application.OpenForms["MathGenForm"].Controls.Find("rdBtnAdditionPositive", true)[0]).Checked));
                        break;
                    case "Add_2":
                        mathOutputList.Add(GenerateAddition(rnd,99, 1000, ((RadioButton)Application.OpenForms["MathGenForm"].Controls.Find("rdBtnAdditionPositive", true)[0]).Checked));
                        break;
                    case "Add_3":
                        mathOutputList.Add(GenerateAddition(rnd,999, 10001, ((RadioButton)Application.OpenForms["MathGenForm"].Controls.Find("rdBtnAdditionPositive", true)[0]).Checked));
                        break;
                    case "Sub_1":
                        mathOutputList.Add(GenerateSubtraction(rnd,0, 100, ((RadioButton)Application.OpenForms["MathGenForm"].Controls.Find("rdBtnAdditionPositive", true)[0]).Checked));
                        break;
                    case "Sub_2":
                        mathOutputList.Add(GenerateSubtraction(rnd,99, 1000, ((RadioButton)Application.OpenForms["MathGenForm"].Controls.Find("rdBtnAdditionPositive", true)[0]).Checked));
                        break;
                    case "Sub_3":
                        mathOutputList.Add(GenerateSubtraction(rnd,999, 10001, ((RadioButton)Application.OpenForms["MathGenForm"].Controls.Find("rdBtnAdditionPositive", true)[0]).Checked));
                        break;
                    case "Multi_1":
                        if(!multiFlag)
                        for (int l = 1; l <= 12; l ++)
                        {
                            mathOutputList.Add(GenerateMultiplication(l,Convert.ToInt32(Application.OpenForms["MathGenForm"].Controls.Find("txtBoxTimesTable", true)[0].Text)));
                        }
                        multiFlag = true;
                        break;
                    //case "Multi_2":
                    //    mathOutputList.Add(GenerateMultiplication(rnd, 1, 100));
                    //    break;
                    //case "Multi_3":
                    //    mathOutputList.Add(GenerateMultiplication(rnd, 1, 10000));
                    //    break;
                    case "LongMul_1":
                        mathOutputList.Add(GenerateLongMultiplication(rnd, Convert.ToInt32(Application.OpenForms["MathGenForm"].Controls.Find("txtLongMultiStartRange", true)[0].Text), Convert.ToInt32(Application.OpenForms["MathGenForm"].Controls.Find("txtLongMultiEndRange", true)[0].Text)));
                        break;
                    case "Algebra_1":
                        mathOutputList.Add(GenerateAlgebra(rnd));
                        break;
                    case "Fractions_1":
                        mathOutputList.Add(GenerateFractions(rnd,1));
                        break;
                    case "Fractions_2":
                        mathOutputList.Add(GenerateFractions(rnd, 2));
                        break;
                    case "Fractions_3":
                        mathOutputList.Add(GenerateFractions(rnd, 3));
                        break;
                    case "Fractions_4":
                        mathOutputList.Add(GenerateFractions(rnd, 4));
                        break;
                    case "LongDiv_1":
                        mathOutputList.Add(GenerateDivisions(rnd,
                            Convert.ToInt32(Application.OpenForms["MathGenForm"].Controls.Find("txtBoxLongDivDividendStartRange", true)[0].Text), 
                            Convert.ToInt32(Application.OpenForms["MathGenForm"].Controls.Find("txtBoxLongDivDividendEndRange", true)[0].Text),
                            Convert.ToInt32(Application.OpenForms["MathGenForm"].Controls.Find("txtBoxLongDivDivisorStartRange", true)[0].Text),
                            Convert.ToInt32(Application.OpenForms["MathGenForm"].Controls.Find("txtBoxLongDivDivisorEndRange", true)[0].Text)
                            ));
                        break;
                }
                //mathOutputList[i].index = i+1;
            }
            return mathOutputList;
        }
        private MathOutput GenerateDivisions(Random rnd,int dividendStartRange, int dividendEndRange, int divisorStartRange, int divisorEndRange)
        {
            int a = rnd.Next(dividendStartRange, dividendEndRange);
            int b = rnd.Next(divisorStartRange, divisorEndRange);
            return new MathOutput {
                OutputMath = a + " ÷ " + b + " =  , Remainder = ",
                Answer = a + " ÷ " + b + " = " + a / b + ", Remainder = " + a % b,
                MathType=MathType.Division
            };
        }
        private MathOutput GenerateAddition(Random rnd,int start, int end,bool positive)
        { 
            int a = rnd.Next(start, end);
            int b = rnd.Next(start, end);
            int c = positive ? 1 : -1;
            return new MathOutput {
                OutputMath = a*c + " + " + b*c + " = ",
                Answer = (a*c + b*c).ToString()
            };
        }
        private MathOutput GenerateAlgebra(Random rnd)
        {
            int a = rnd.Next(1, 10);
            int b = rnd.Next(1, 10);
            int c = a * b;
            int op = rnd.Next(1, 3);
            //+
            if (op == 1)
            {
                int d = c + rnd.Next(1, c);
                return new MathOutput
                {
                    OutputMath = a + "x " + "+" + " " + (d - c) + " = " + d,
                    Answer = (b).ToString(),
                    MathType = MathType.Algebra
                };
            }
            //-
            else
            {
                int d = c - rnd.Next(1, c);
                return new MathOutput
                {
                    OutputMath = a + "x " + "-" + " " + (c-d) + " = " + d,
                    Answer = (b).ToString(),
                    MathType = MathType.Algebra
                };
            }            
        }
        private MathOutput GenerateFractions(Random rnd,int op)
        {
            int a = rnd.Next(1, 9);
            int b = rnd.Next(a+1,9);
            int c = rnd.Next(1, 9);
            int d = rnd.Next(c + 1, 9);
            Fraction af = new Fraction(a, b);
            Fraction cf = new Fraction(c, d);
            FractionOperator fractionOperator=FractionOperator.Add;
            
            //+
            switch (op)
            {
                //+
                case 1:
                    af = af + cf;
                    fractionOperator = FractionOperator.Add;
                    break;
                //-
                case 2:
                    af = af - cf;
                    fractionOperator = FractionOperator.Substract;
                    break;
                //x
                case 3:
                    af = af * cf;
                    fractionOperator = FractionOperator.Multiplication;
                    break;
                //divide
                case 4:
                    af = af / cf;
                    fractionOperator = FractionOperator.Division;
                    break;
            }
            return new MathOutput {
                OutputFraction = new FractionOutput { Num1 = a, Num2 = c, Denom1 = b, Denom2 = d },
                AnswerFraction=new FractionOutput {Num1=Convert.ToInt32(af.Numerator),Denom1=Convert.ToInt32(af.Denominator) },
                FractionOperator=fractionOperator,
                MathType=MathType.Fraction
            };
        }
        private MathOutput GenerateLongMultiplication(Random rnd, int start, int end)
        {
            int a = rnd.Next(start, end);
            int b = rnd.Next(start, end);
            return new MathOutput
            {
                OutputMath = a + "|" + b,
                Answer = (a * b).ToString(),
                MathType = MathType.Multiplication
            };
        }
        private MathOutput GenerateSubtraction(Random rnd, int start, int end, bool positive)
        {
            int a = rnd.Next(start, end);
            int b = rnd.Next(start, end);
            int c = positive ? 1 : -1;
            return new MathOutput
            {
                OutputMath = a*c + " - " + b*c + " = ",
                Answer = (a*c - b*c).ToString()
            };
        }
        private MathOutput GenerateMultiplication(int number,int times)
        {
            return new MathOutput
            {
                OutputMath = number + " × " + times + " =  ",
                Answer = (number * times).ToString()               
            };
        }
    }
    public class MathOutput
    {
        public String OutputMath;
        public String Answer { get; set; }
        public MathType MathType { get; set; }
        public FractionOutput OutputFraction { get; set; }
        public FractionOutput AnswerFraction { get; set; }
        public FractionOperator FractionOperator{get;set;}
        public int index;
        public MathOutput()
        {
            OutputMath = "";
            Answer = "";
            MathType = MathType.Default;
            index = 0;
        }
    }
    public class FractionOutput
    {
        public int Num1 { get; set; }
        public int Denom1 { get; set; }
        public int Num2 { get; set; }
        public int Denom2 { get; set; }
    }
        public enum MathType
    {
        Default=1,
        Multiplication=2,
        Algebra=3,
        Fraction=4,
        Division=5
    }
    public enum FractionOperator
    {
        Add=1,
        Substract=2,
        Multiplication=3,
        Division=4
    }
}
