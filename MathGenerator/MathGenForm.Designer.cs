﻿namespace MathGenerator
{
    partial class MathGenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MathGenForm));
            this.tab_MathControl = new System.Windows.Forms.TabControl();
            this.tab_Addition = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chkBoxSub999To10000 = new System.Windows.Forms.CheckBox();
            this.lblSubtraction = new System.Windows.Forms.Label();
            this.chkBoxSub0To99 = new System.Windows.Forms.CheckBox();
            this.chkBoxSub99To999 = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkBoxAdd999To10000 = new System.Windows.Forms.CheckBox();
            this.lblAddition = new System.Windows.Forms.Label();
            this.chkBoxAdd0To99 = new System.Windows.Forms.CheckBox();
            this.chkBoxAdd99To999 = new System.Windows.Forms.CheckBox();
            this.tab_Multiplication = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtBoxTimesTable = new System.Windows.Forms.TextBox();
            this.lblTimes = new System.Windows.Forms.Label();
            this.lblMultiplication = new System.Windows.Forms.Label();
            this.chkBoxMul1To12 = new System.Windows.Forms.CheckBox();
            this.tab_longMulti = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblLongMultiEndRage = new System.Windows.Forms.Label();
            this.txtLongMultiEndRange = new System.Windows.Forms.TextBox();
            this.lblLongMultiStartRange = new System.Windows.Forms.Label();
            this.txtLongMultiStartRange = new System.Windows.Forms.TextBox();
            this.chkBoxLongMulti = new System.Windows.Forms.CheckBox();
            this.tab_longDivision = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblLongDivDivisor = new System.Windows.Forms.Label();
            this.lblLongDivDivisorEndRange = new System.Windows.Forms.Label();
            this.txtBoxLongDivDivisorEndRange = new System.Windows.Forms.TextBox();
            this.lblLongDivDivisorStartRange = new System.Windows.Forms.Label();
            this.txtBoxLongDivDivisorStartRange = new System.Windows.Forms.TextBox();
            this.lblLongDivDividend = new System.Windows.Forms.Label();
            this.lblLongDivDividendEndRange = new System.Windows.Forms.Label();
            this.txtBoxLongDivDividendEndRange = new System.Windows.Forms.TextBox();
            this.lblLongDivDividendStartRange = new System.Windows.Forms.Label();
            this.txtBoxLongDivDividendStartRange = new System.Windows.Forms.TextBox();
            this.chkBoxLongDivision = new System.Windows.Forms.CheckBox();
            this.tab_fractions = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblFractions = new System.Windows.Forms.Label();
            this.chkBoxFractionsDivide = new System.Windows.Forms.CheckBox();
            this.chkBoxFractionsMultiply = new System.Windows.Forms.CheckBox();
            this.chkBoxFractionsSub = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkBoxFractionsSum = new System.Windows.Forms.CheckBox();
            this.tab_algebra = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.chkBoxAlgebra = new System.Windows.Forms.CheckBox();
            this.lblAlgebra = new System.Windows.Forms.Label();
            this.btn_generate = new System.Windows.Forms.Button();
            this.combo_layout = new System.Windows.Forms.ComboBox();
            this.lblLayout = new System.Windows.Forms.Label();
            this.lblQuestions = new System.Windows.Forms.Label();
            this.txtQuestions = new System.Windows.Forms.TextBox();
            this.lblMathGen = new System.Windows.Forms.Label();
            this.rdBtnAdditionPositive = new System.Windows.Forms.RadioButton();
            this.rdBtnAdditionNegative = new System.Windows.Forms.RadioButton();
            this.tab_MathControl.SuspendLayout();
            this.tab_Addition.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tab_Multiplication.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tab_longMulti.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tab_longDivision.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tab_fractions.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tab_algebra.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab_MathControl
            // 
            this.tab_MathControl.Controls.Add(this.tab_Addition);
            this.tab_MathControl.Controls.Add(this.tab_Multiplication);
            this.tab_MathControl.Controls.Add(this.tab_longMulti);
            this.tab_MathControl.Controls.Add(this.tab_longDivision);
            this.tab_MathControl.Controls.Add(this.tab_fractions);
            this.tab_MathControl.Controls.Add(this.tab_algebra);
            this.tab_MathControl.Location = new System.Drawing.Point(13, 57);
            this.tab_MathControl.Name = "tab_MathControl";
            this.tab_MathControl.SelectedIndex = 0;
            this.tab_MathControl.Size = new System.Drawing.Size(1153, 351);
            this.tab_MathControl.TabIndex = 0;
            // 
            // tab_Addition
            // 
            this.tab_Addition.Controls.Add(this.rdBtnAdditionNegative);
            this.tab_Addition.Controls.Add(this.rdBtnAdditionPositive);
            this.tab_Addition.Controls.Add(this.panel2);
            this.tab_Addition.Controls.Add(this.panel1);
            this.tab_Addition.Location = new System.Drawing.Point(4, 29);
            this.tab_Addition.Name = "tab_Addition";
            this.tab_Addition.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Addition.Size = new System.Drawing.Size(1145, 318);
            this.tab_Addition.TabIndex = 0;
            this.tab_Addition.Text = "Addition & Subtraction";
            this.tab_Addition.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.chkBoxSub999To10000);
            this.panel2.Controls.Add(this.lblSubtraction);
            this.panel2.Controls.Add(this.chkBoxSub0To99);
            this.panel2.Controls.Add(this.chkBoxSub99To999);
            this.panel2.Location = new System.Drawing.Point(315, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(262, 268);
            this.panel2.TabIndex = 3;
            // 
            // chkBoxSub999To10000
            // 
            this.chkBoxSub999To10000.AutoSize = true;
            this.chkBoxSub999To10000.Location = new System.Drawing.Point(20, 100);
            this.chkBoxSub999To10000.Name = "chkBoxSub999To10000";
            this.chkBoxSub999To10000.Size = new System.Drawing.Size(112, 24);
            this.chkBoxSub999To10000.TabIndex = 3;
            this.chkBoxSub999To10000.Tag = "Sub_3";
            this.chkBoxSub999To10000.Text = "999-10000";
            this.chkBoxSub999To10000.UseVisualStyleBackColor = true;
            // 
            // lblSubtraction
            // 
            this.lblSubtraction.AutoSize = true;
            this.lblSubtraction.Location = new System.Drawing.Point(20, 12);
            this.lblSubtraction.Name = "lblSubtraction";
            this.lblSubtraction.Size = new System.Drawing.Size(91, 20);
            this.lblSubtraction.TabIndex = 2;
            this.lblSubtraction.Text = "Subtraction";
            // 
            // chkBoxSub0To99
            // 
            this.chkBoxSub0To99.AutoSize = true;
            this.chkBoxSub0To99.Location = new System.Drawing.Point(20, 38);
            this.chkBoxSub0To99.Name = "chkBoxSub0To99";
            this.chkBoxSub0To99.Size = new System.Drawing.Size(67, 24);
            this.chkBoxSub0To99.TabIndex = 0;
            this.chkBoxSub0To99.Tag = "Sub_1";
            this.chkBoxSub0To99.Text = "0-99";
            this.chkBoxSub0To99.UseVisualStyleBackColor = true;
            // 
            // chkBoxSub99To999
            // 
            this.chkBoxSub99To999.AutoSize = true;
            this.chkBoxSub99To999.Location = new System.Drawing.Point(20, 69);
            this.chkBoxSub99To999.Name = "chkBoxSub99To999";
            this.chkBoxSub99To999.Size = new System.Drawing.Size(85, 24);
            this.chkBoxSub99To999.TabIndex = 1;
            this.chkBoxSub99To999.Tag = "Sub_2";
            this.chkBoxSub99To999.Text = "99-999";
            this.chkBoxSub99To999.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkBoxAdd999To10000);
            this.panel1.Controls.Add(this.lblAddition);
            this.panel1.Controls.Add(this.chkBoxAdd0To99);
            this.panel1.Controls.Add(this.chkBoxAdd99To999);
            this.panel1.Location = new System.Drawing.Point(24, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(262, 268);
            this.panel1.TabIndex = 2;
            // 
            // chkBoxAdd999To10000
            // 
            this.chkBoxAdd999To10000.AutoSize = true;
            this.chkBoxAdd999To10000.Location = new System.Drawing.Point(20, 100);
            this.chkBoxAdd999To10000.Name = "chkBoxAdd999To10000";
            this.chkBoxAdd999To10000.Size = new System.Drawing.Size(112, 24);
            this.chkBoxAdd999To10000.TabIndex = 3;
            this.chkBoxAdd999To10000.Tag = "Add_3";
            this.chkBoxAdd999To10000.Text = "999-10000";
            this.chkBoxAdd999To10000.UseVisualStyleBackColor = true;
            // 
            // lblAddition
            // 
            this.lblAddition.AutoSize = true;
            this.lblAddition.Location = new System.Drawing.Point(20, 12);
            this.lblAddition.Name = "lblAddition";
            this.lblAddition.Size = new System.Drawing.Size(67, 20);
            this.lblAddition.TabIndex = 2;
            this.lblAddition.Text = "Addition";
            // 
            // chkBoxAdd0To99
            // 
            this.chkBoxAdd0To99.AutoSize = true;
            this.chkBoxAdd0To99.Location = new System.Drawing.Point(20, 38);
            this.chkBoxAdd0To99.Name = "chkBoxAdd0To99";
            this.chkBoxAdd0To99.Size = new System.Drawing.Size(67, 24);
            this.chkBoxAdd0To99.TabIndex = 0;
            this.chkBoxAdd0To99.Tag = "Add_1";
            this.chkBoxAdd0To99.Text = "0-99";
            this.chkBoxAdd0To99.UseVisualStyleBackColor = true;
            // 
            // chkBoxAdd99To999
            // 
            this.chkBoxAdd99To999.AutoSize = true;
            this.chkBoxAdd99To999.Location = new System.Drawing.Point(20, 69);
            this.chkBoxAdd99To999.Name = "chkBoxAdd99To999";
            this.chkBoxAdd99To999.Size = new System.Drawing.Size(85, 24);
            this.chkBoxAdd99To999.TabIndex = 1;
            this.chkBoxAdd99To999.Tag = "Add_2";
            this.chkBoxAdd99To999.Text = "99-999";
            this.chkBoxAdd99To999.UseVisualStyleBackColor = true;
            // 
            // tab_Multiplication
            // 
            this.tab_Multiplication.Controls.Add(this.panel3);
            this.tab_Multiplication.Location = new System.Drawing.Point(4, 29);
            this.tab_Multiplication.Name = "tab_Multiplication";
            this.tab_Multiplication.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Multiplication.Size = new System.Drawing.Size(1145, 318);
            this.tab_Multiplication.TabIndex = 1;
            this.tab_Multiplication.Text = "Multiplication";
            this.tab_Multiplication.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtBoxTimesTable);
            this.panel3.Controls.Add(this.lblTimes);
            this.panel3.Controls.Add(this.lblMultiplication);
            this.panel3.Controls.Add(this.chkBoxMul1To12);
            this.panel3.Location = new System.Drawing.Point(24, 19);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(262, 268);
            this.panel3.TabIndex = 3;
            // 
            // txtBoxTimesTable
            // 
            this.txtBoxTimesTable.Location = new System.Drawing.Point(20, 93);
            this.txtBoxTimesTable.Name = "txtBoxTimesTable";
            this.txtBoxTimesTable.Size = new System.Drawing.Size(100, 26);
            this.txtBoxTimesTable.TabIndex = 4;
            // 
            // lblTimes
            // 
            this.lblTimes.AutoSize = true;
            this.lblTimes.Location = new System.Drawing.Point(20, 69);
            this.lblTimes.Name = "lblTimes";
            this.lblTimes.Size = new System.Drawing.Size(94, 20);
            this.lblTimes.TabIndex = 3;
            this.lblTimes.Text = "Times Table";
            // 
            // lblMultiplication
            // 
            this.lblMultiplication.AutoSize = true;
            this.lblMultiplication.Location = new System.Drawing.Point(20, 12);
            this.lblMultiplication.Name = "lblMultiplication";
            this.lblMultiplication.Size = new System.Drawing.Size(100, 20);
            this.lblMultiplication.TabIndex = 2;
            this.lblMultiplication.Text = "Multiplication";
            // 
            // chkBoxMul1To12
            // 
            this.chkBoxMul1To12.AutoSize = true;
            this.chkBoxMul1To12.Location = new System.Drawing.Point(20, 38);
            this.chkBoxMul1To12.Name = "chkBoxMul1To12";
            this.chkBoxMul1To12.Size = new System.Drawing.Size(67, 24);
            this.chkBoxMul1To12.TabIndex = 0;
            this.chkBoxMul1To12.Tag = "Multi_1";
            this.chkBoxMul1To12.Text = "1-12";
            this.chkBoxMul1To12.UseVisualStyleBackColor = true;
            // 
            // tab_longMulti
            // 
            this.tab_longMulti.Controls.Add(this.panel4);
            this.tab_longMulti.Location = new System.Drawing.Point(4, 29);
            this.tab_longMulti.Name = "tab_longMulti";
            this.tab_longMulti.Padding = new System.Windows.Forms.Padding(3);
            this.tab_longMulti.Size = new System.Drawing.Size(1145, 318);
            this.tab_longMulti.TabIndex = 2;
            this.tab_longMulti.Text = "Long Multiplication";
            this.tab_longMulti.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblLongMultiEndRage);
            this.panel4.Controls.Add(this.txtLongMultiEndRange);
            this.panel4.Controls.Add(this.lblLongMultiStartRange);
            this.panel4.Controls.Add(this.txtLongMultiStartRange);
            this.panel4.Controls.Add(this.chkBoxLongMulti);
            this.panel4.Location = new System.Drawing.Point(21, 15);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(306, 268);
            this.panel4.TabIndex = 3;
            // 
            // lblLongMultiEndRage
            // 
            this.lblLongMultiEndRage.AutoSize = true;
            this.lblLongMultiEndRage.Location = new System.Drawing.Point(10, 76);
            this.lblLongMultiEndRage.Name = "lblLongMultiEndRage";
            this.lblLongMultiEndRage.Size = new System.Drawing.Size(90, 20);
            this.lblLongMultiEndRage.TabIndex = 4;
            this.lblLongMultiEndRage.Text = "End Range";
            // 
            // txtLongMultiEndRange
            // 
            this.txtLongMultiEndRange.Location = new System.Drawing.Point(112, 73);
            this.txtLongMultiEndRange.Name = "txtLongMultiEndRange";
            this.txtLongMultiEndRange.Size = new System.Drawing.Size(130, 26);
            this.txtLongMultiEndRange.TabIndex = 3;
            // 
            // lblLongMultiStartRange
            // 
            this.lblLongMultiStartRange.AutoSize = true;
            this.lblLongMultiStartRange.Location = new System.Drawing.Point(10, 44);
            this.lblLongMultiStartRange.Name = "lblLongMultiStartRange";
            this.lblLongMultiStartRange.Size = new System.Drawing.Size(96, 20);
            this.lblLongMultiStartRange.TabIndex = 2;
            this.lblLongMultiStartRange.Text = "Start Range";
            // 
            // txtLongMultiStartRange
            // 
            this.txtLongMultiStartRange.Location = new System.Drawing.Point(112, 41);
            this.txtLongMultiStartRange.Name = "txtLongMultiStartRange";
            this.txtLongMultiStartRange.Size = new System.Drawing.Size(130, 26);
            this.txtLongMultiStartRange.TabIndex = 1;
            // 
            // chkBoxLongMulti
            // 
            this.chkBoxLongMulti.AutoSize = true;
            this.chkBoxLongMulti.Location = new System.Drawing.Point(14, 14);
            this.chkBoxLongMulti.Name = "chkBoxLongMulti";
            this.chkBoxLongMulti.Size = new System.Drawing.Size(166, 24);
            this.chkBoxLongMulti.TabIndex = 0;
            this.chkBoxLongMulti.Tag = "LongMul_1";
            this.chkBoxLongMulti.Text = "Long Multiplication";
            this.chkBoxLongMulti.UseVisualStyleBackColor = true;
            // 
            // tab_longDivision
            // 
            this.tab_longDivision.Controls.Add(this.panel5);
            this.tab_longDivision.Location = new System.Drawing.Point(4, 29);
            this.tab_longDivision.Name = "tab_longDivision";
            this.tab_longDivision.Size = new System.Drawing.Size(1145, 318);
            this.tab_longDivision.TabIndex = 3;
            this.tab_longDivision.Text = "Long Division";
            this.tab_longDivision.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblLongDivDivisor);
            this.panel5.Controls.Add(this.lblLongDivDivisorEndRange);
            this.panel5.Controls.Add(this.txtBoxLongDivDivisorEndRange);
            this.panel5.Controls.Add(this.lblLongDivDivisorStartRange);
            this.panel5.Controls.Add(this.txtBoxLongDivDivisorStartRange);
            this.panel5.Controls.Add(this.lblLongDivDividend);
            this.panel5.Controls.Add(this.lblLongDivDividendEndRange);
            this.panel5.Controls.Add(this.txtBoxLongDivDividendEndRange);
            this.panel5.Controls.Add(this.lblLongDivDividendStartRange);
            this.panel5.Controls.Add(this.txtBoxLongDivDividendStartRange);
            this.panel5.Controls.Add(this.chkBoxLongDivision);
            this.panel5.Location = new System.Drawing.Point(14, 15);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(262, 268);
            this.panel5.TabIndex = 4;
            // 
            // lblLongDivDivisor
            // 
            this.lblLongDivDivisor.AutoSize = true;
            this.lblLongDivDivisor.Location = new System.Drawing.Point(49, 150);
            this.lblLongDivDivisor.Name = "lblLongDivDivisor";
            this.lblLongDivDivisor.Size = new System.Drawing.Size(56, 20);
            this.lblLongDivDivisor.TabIndex = 10;
            this.lblLongDivDivisor.Text = "Divisor";
            // 
            // lblLongDivDivisorEndRange
            // 
            this.lblLongDivDivisorEndRange.AutoSize = true;
            this.lblLongDivDivisorEndRange.Location = new System.Drawing.Point(10, 211);
            this.lblLongDivDivisorEndRange.Name = "lblLongDivDivisorEndRange";
            this.lblLongDivDivisorEndRange.Size = new System.Drawing.Size(90, 20);
            this.lblLongDivDivisorEndRange.TabIndex = 9;
            this.lblLongDivDivisorEndRange.Text = "End Range";
            // 
            // txtBoxLongDivDivisorEndRange
            // 
            this.txtBoxLongDivDivisorEndRange.Location = new System.Drawing.Point(112, 208);
            this.txtBoxLongDivDivisorEndRange.Name = "txtBoxLongDivDivisorEndRange";
            this.txtBoxLongDivDivisorEndRange.Size = new System.Drawing.Size(130, 26);
            this.txtBoxLongDivDivisorEndRange.TabIndex = 8;
            // 
            // lblLongDivDivisorStartRange
            // 
            this.lblLongDivDivisorStartRange.AutoSize = true;
            this.lblLongDivDivisorStartRange.Location = new System.Drawing.Point(10, 179);
            this.lblLongDivDivisorStartRange.Name = "lblLongDivDivisorStartRange";
            this.lblLongDivDivisorStartRange.Size = new System.Drawing.Size(96, 20);
            this.lblLongDivDivisorStartRange.TabIndex = 7;
            this.lblLongDivDivisorStartRange.Text = "Start Range";
            // 
            // txtBoxLongDivDivisorStartRange
            // 
            this.txtBoxLongDivDivisorStartRange.Location = new System.Drawing.Point(112, 176);
            this.txtBoxLongDivDivisorStartRange.Name = "txtBoxLongDivDivisorStartRange";
            this.txtBoxLongDivDivisorStartRange.Size = new System.Drawing.Size(130, 26);
            this.txtBoxLongDivDivisorStartRange.TabIndex = 6;
            // 
            // lblLongDivDividend
            // 
            this.lblLongDivDividend.AutoSize = true;
            this.lblLongDivDividend.Location = new System.Drawing.Point(49, 55);
            this.lblLongDivDividend.Name = "lblLongDivDividend";
            this.lblLongDivDividend.Size = new System.Drawing.Size(70, 20);
            this.lblLongDivDividend.TabIndex = 5;
            this.lblLongDivDividend.Text = "Dividend";
            // 
            // lblLongDivDividendEndRange
            // 
            this.lblLongDivDividendEndRange.AutoSize = true;
            this.lblLongDivDividendEndRange.Location = new System.Drawing.Point(10, 120);
            this.lblLongDivDividendEndRange.Name = "lblLongDivDividendEndRange";
            this.lblLongDivDividendEndRange.Size = new System.Drawing.Size(90, 20);
            this.lblLongDivDividendEndRange.TabIndex = 4;
            this.lblLongDivDividendEndRange.Text = "End Range";
            // 
            // txtBoxLongDivDividendEndRange
            // 
            this.txtBoxLongDivDividendEndRange.Location = new System.Drawing.Point(112, 117);
            this.txtBoxLongDivDividendEndRange.Name = "txtBoxLongDivDividendEndRange";
            this.txtBoxLongDivDividendEndRange.Size = new System.Drawing.Size(130, 26);
            this.txtBoxLongDivDividendEndRange.TabIndex = 3;
            // 
            // lblLongDivDividendStartRange
            // 
            this.lblLongDivDividendStartRange.AutoSize = true;
            this.lblLongDivDividendStartRange.Location = new System.Drawing.Point(10, 88);
            this.lblLongDivDividendStartRange.Name = "lblLongDivDividendStartRange";
            this.lblLongDivDividendStartRange.Size = new System.Drawing.Size(96, 20);
            this.lblLongDivDividendStartRange.TabIndex = 2;
            this.lblLongDivDividendStartRange.Text = "Start Range";
            // 
            // txtBoxLongDivDividendStartRange
            // 
            this.txtBoxLongDivDividendStartRange.Location = new System.Drawing.Point(112, 85);
            this.txtBoxLongDivDividendStartRange.Name = "txtBoxLongDivDividendStartRange";
            this.txtBoxLongDivDividendStartRange.Size = new System.Drawing.Size(130, 26);
            this.txtBoxLongDivDividendStartRange.TabIndex = 1;
            // 
            // chkBoxLongDivision
            // 
            this.chkBoxLongDivision.AutoSize = true;
            this.chkBoxLongDivision.Location = new System.Drawing.Point(14, 14);
            this.chkBoxLongDivision.Name = "chkBoxLongDivision";
            this.chkBoxLongDivision.Size = new System.Drawing.Size(129, 24);
            this.chkBoxLongDivision.TabIndex = 0;
            this.chkBoxLongDivision.Tag = "LongDiv_1";
            this.chkBoxLongDivision.Text = "Long Division";
            this.chkBoxLongDivision.UseVisualStyleBackColor = true;
            // 
            // tab_fractions
            // 
            this.tab_fractions.Controls.Add(this.panel6);
            this.tab_fractions.Location = new System.Drawing.Point(4, 29);
            this.tab_fractions.Name = "tab_fractions";
            this.tab_fractions.Size = new System.Drawing.Size(1145, 318);
            this.tab_fractions.TabIndex = 4;
            this.tab_fractions.Text = "Fractions";
            this.tab_fractions.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lblFractions);
            this.panel6.Controls.Add(this.chkBoxFractionsDivide);
            this.panel6.Controls.Add(this.chkBoxFractionsMultiply);
            this.panel6.Controls.Add(this.chkBoxFractionsSub);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Controls.Add(this.chkBoxFractionsSum);
            this.panel6.Location = new System.Drawing.Point(13, 15);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(306, 268);
            this.panel6.TabIndex = 4;
            // 
            // lblFractions
            // 
            this.lblFractions.AutoSize = true;
            this.lblFractions.Location = new System.Drawing.Point(15, 18);
            this.lblFractions.Name = "lblFractions";
            this.lblFractions.Size = new System.Drawing.Size(75, 20);
            this.lblFractions.TabIndex = 5;
            this.lblFractions.Text = "Fractions";
            // 
            // chkBoxFractionsDivide
            // 
            this.chkBoxFractionsDivide.AutoSize = true;
            this.chkBoxFractionsDivide.Location = new System.Drawing.Point(14, 173);
            this.chkBoxFractionsDivide.Name = "chkBoxFractionsDivide";
            this.chkBoxFractionsDivide.Size = new System.Drawing.Size(89, 24);
            this.chkBoxFractionsDivide.TabIndex = 4;
            this.chkBoxFractionsDivide.Tag = "Fractions_4";
            this.chkBoxFractionsDivide.Text = "Division";
            this.chkBoxFractionsDivide.UseVisualStyleBackColor = true;
            // 
            // chkBoxFractionsMultiply
            // 
            this.chkBoxFractionsMultiply.AutoSize = true;
            this.chkBoxFractionsMultiply.Location = new System.Drawing.Point(14, 143);
            this.chkBoxFractionsMultiply.Name = "chkBoxFractionsMultiply";
            this.chkBoxFractionsMultiply.Size = new System.Drawing.Size(87, 24);
            this.chkBoxFractionsMultiply.TabIndex = 3;
            this.chkBoxFractionsMultiply.Tag = "Fractions_3";
            this.chkBoxFractionsMultiply.Text = "Multiply";
            this.chkBoxFractionsMultiply.UseVisualStyleBackColor = true;
            // 
            // chkBoxFractionsSub
            // 
            this.chkBoxFractionsSub.AutoSize = true;
            this.chkBoxFractionsSub.Location = new System.Drawing.Point(14, 113);
            this.chkBoxFractionsSub.Name = "chkBoxFractionsSub";
            this.chkBoxFractionsSub.Size = new System.Drawing.Size(125, 24);
            this.chkBoxFractionsSub.TabIndex = 2;
            this.chkBoxFractionsSub.Tag = "Fractions_2";
            this.chkBoxFractionsSub.Text = "Substraction";
            this.chkBoxFractionsSub.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Denominators 1-10";
            // 
            // chkBoxFractionsSum
            // 
            this.chkBoxFractionsSum.AutoSize = true;
            this.chkBoxFractionsSum.Location = new System.Drawing.Point(14, 83);
            this.chkBoxFractionsSum.Name = "chkBoxFractionsSum";
            this.chkBoxFractionsSum.Size = new System.Drawing.Size(68, 24);
            this.chkBoxFractionsSum.TabIndex = 0;
            this.chkBoxFractionsSum.Tag = "Fractions_1";
            this.chkBoxFractionsSum.Text = "Sum";
            this.chkBoxFractionsSum.UseVisualStyleBackColor = true;
            // 
            // tab_algebra
            // 
            this.tab_algebra.Controls.Add(this.panel7);
            this.tab_algebra.Location = new System.Drawing.Point(4, 29);
            this.tab_algebra.Name = "tab_algebra";
            this.tab_algebra.Size = new System.Drawing.Size(1145, 318);
            this.tab_algebra.TabIndex = 5;
            this.tab_algebra.Text = "Algebra";
            this.tab_algebra.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.chkBoxAlgebra);
            this.panel7.Controls.Add(this.lblAlgebra);
            this.panel7.Location = new System.Drawing.Point(15, 12);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(262, 268);
            this.panel7.TabIndex = 4;
            // 
            // chkBoxAlgebra
            // 
            this.chkBoxAlgebra.AutoSize = true;
            this.chkBoxAlgebra.Location = new System.Drawing.Point(24, 48);
            this.chkBoxAlgebra.Name = "chkBoxAlgebra";
            this.chkBoxAlgebra.Size = new System.Drawing.Size(134, 24);
            this.chkBoxAlgebra.TabIndex = 3;
            this.chkBoxAlgebra.Tag = "Algebra_1";
            this.chkBoxAlgebra.Text = "Type 4x+8=12";
            this.chkBoxAlgebra.UseVisualStyleBackColor = true;
            // 
            // lblAlgebra
            // 
            this.lblAlgebra.AutoSize = true;
            this.lblAlgebra.Location = new System.Drawing.Point(20, 12);
            this.lblAlgebra.Name = "lblAlgebra";
            this.lblAlgebra.Size = new System.Drawing.Size(64, 20);
            this.lblAlgebra.TabIndex = 2;
            this.lblAlgebra.Text = "Algebra";
            // 
            // btn_generate
            // 
            this.btn_generate.Location = new System.Drawing.Point(518, 476);
            this.btn_generate.Name = "btn_generate";
            this.btn_generate.Size = new System.Drawing.Size(143, 32);
            this.btn_generate.TabIndex = 1;
            this.btn_generate.Text = "Generate";
            this.btn_generate.UseVisualStyleBackColor = true;
            this.btn_generate.Click += new System.EventHandler(this.btn_generate_Click);
            // 
            // combo_layout
            // 
            this.combo_layout.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_layout.FormattingEnabled = true;
            this.combo_layout.Items.AddRange(new object[] {
            "2",
            "3"});
            this.combo_layout.Location = new System.Drawing.Point(134, 433);
            this.combo_layout.Name = "combo_layout";
            this.combo_layout.Size = new System.Drawing.Size(84, 28);
            this.combo_layout.TabIndex = 2;
            // 
            // lblLayout
            // 
            this.lblLayout.AutoSize = true;
            this.lblLayout.Location = new System.Drawing.Point(13, 436);
            this.lblLayout.Name = "lblLayout";
            this.lblLayout.Size = new System.Drawing.Size(107, 20);
            this.lblLayout.TabIndex = 3;
            this.lblLayout.Text = "PDF Columns";
            // 
            // lblQuestions
            // 
            this.lblQuestions.AutoSize = true;
            this.lblQuestions.Location = new System.Drawing.Point(224, 436);
            this.lblQuestions.Name = "lblQuestions";
            this.lblQuestions.Size = new System.Drawing.Size(159, 20);
            this.lblQuestions.TabIndex = 4;
            this.lblQuestions.Text = "Number of Questions";
            // 
            // txtQuestions
            // 
            this.txtQuestions.Location = new System.Drawing.Point(389, 433);
            this.txtQuestions.Name = "txtQuestions";
            this.txtQuestions.Size = new System.Drawing.Size(82, 26);
            this.txtQuestions.TabIndex = 5;
            this.txtQuestions.Text = "0";
            // 
            // lblMathGen
            // 
            this.lblMathGen.AutoSize = true;
            this.lblMathGen.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMathGen.Location = new System.Drawing.Point(433, 9);
            this.lblMathGen.Name = "lblMathGen";
            this.lblMathGen.Size = new System.Drawing.Size(298, 46);
            this.lblMathGen.TabIndex = 6;
            this.lblMathGen.Text = "Math Generator";
            // 
            // rdBtnAdditionPositive
            // 
            this.rdBtnAdditionPositive.AutoSize = true;
            this.rdBtnAdditionPositive.Location = new System.Drawing.Point(646, 87);
            this.rdBtnAdditionPositive.Name = "rdBtnAdditionPositive";
            this.rdBtnAdditionPositive.Size = new System.Drawing.Size(88, 24);
            this.rdBtnAdditionPositive.TabIndex = 4;
            this.rdBtnAdditionPositive.TabStop = true;
            this.rdBtnAdditionPositive.Text = "Positive";
            this.rdBtnAdditionPositive.UseVisualStyleBackColor = true;
            // 
            // rdBtnAdditionNegative
            // 
            this.rdBtnAdditionNegative.AutoSize = true;
            this.rdBtnAdditionNegative.Location = new System.Drawing.Point(646, 118);
            this.rdBtnAdditionNegative.Name = "rdBtnAdditionNegative";
            this.rdBtnAdditionNegative.Size = new System.Drawing.Size(96, 24);
            this.rdBtnAdditionNegative.TabIndex = 5;
            this.rdBtnAdditionNegative.TabStop = true;
            this.rdBtnAdditionNegative.Text = "Negative";
            this.rdBtnAdditionNegative.UseVisualStyleBackColor = true;
            // 
            // MathGenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1178, 539);
            this.Controls.Add(this.lblMathGen);
            this.Controls.Add(this.txtQuestions);
            this.Controls.Add(this.lblQuestions);
            this.Controls.Add(this.lblLayout);
            this.Controls.Add(this.combo_layout);
            this.Controls.Add(this.btn_generate);
            this.Controls.Add(this.tab_MathControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MathGenForm";
            this.Text = "MathGenerator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tab_MathControl.ResumeLayout(false);
            this.tab_Addition.ResumeLayout(false);
            this.tab_Addition.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tab_Multiplication.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tab_longMulti.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tab_longDivision.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tab_fractions.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tab_algebra.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tab_MathControl;
        private System.Windows.Forms.TabPage tab_Addition;
        private System.Windows.Forms.TabPage tab_Multiplication;
        private System.Windows.Forms.TabPage tab_longMulti;
        private System.Windows.Forms.TabPage tab_longDivision;
        private System.Windows.Forms.TabPage tab_fractions;
        private System.Windows.Forms.TabPage tab_algebra;
        private System.Windows.Forms.Button btn_generate;
        private System.Windows.Forms.ComboBox combo_layout;
        private System.Windows.Forms.Label lblLayout;
        private System.Windows.Forms.CheckBox chkBoxAdd0To99;
        private System.Windows.Forms.CheckBox chkBoxAdd99To999;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblAddition;
        private System.Windows.Forms.CheckBox chkBoxAdd999To10000;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chkBoxSub999To10000;
        private System.Windows.Forms.Label lblSubtraction;
        private System.Windows.Forms.CheckBox chkBoxSub0To99;
        private System.Windows.Forms.CheckBox chkBoxSub99To999;
        private System.Windows.Forms.Label lblQuestions;
        private System.Windows.Forms.TextBox txtQuestions;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblMultiplication;
        private System.Windows.Forms.CheckBox chkBoxMul1To12;
        private System.Windows.Forms.Label lblMathGen;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.CheckBox chkBoxLongMulti;
        private System.Windows.Forms.TextBox txtLongMultiStartRange;
        private System.Windows.Forms.Label lblLongMultiStartRange;
        private System.Windows.Forms.Label lblLongMultiEndRage;
        private System.Windows.Forms.TextBox txtLongMultiEndRange;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblLongDivDividendEndRange;
        private System.Windows.Forms.TextBox txtBoxLongDivDividendEndRange;
        private System.Windows.Forms.Label lblLongDivDividendStartRange;
        private System.Windows.Forms.TextBox txtBoxLongDivDividendStartRange;
        private System.Windows.Forms.CheckBox chkBoxLongDivision;
        private System.Windows.Forms.Label lblLongDivDividend;
        private System.Windows.Forms.Label lblLongDivDivisor;
        private System.Windows.Forms.Label lblLongDivDivisorEndRange;
        private System.Windows.Forms.TextBox txtBoxLongDivDivisorEndRange;
        private System.Windows.Forms.Label lblLongDivDivisorStartRange;
        private System.Windows.Forms.TextBox txtBoxLongDivDivisorStartRange;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.CheckBox chkBoxFractionsSum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblFractions;
        private System.Windows.Forms.CheckBox chkBoxFractionsDivide;
        private System.Windows.Forms.CheckBox chkBoxFractionsMultiply;
        private System.Windows.Forms.CheckBox chkBoxFractionsSub;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lblAlgebra;
        private System.Windows.Forms.CheckBox chkBoxAlgebra;
        private System.Windows.Forms.TextBox txtBoxTimesTable;
        private System.Windows.Forms.Label lblTimes;
        private System.Windows.Forms.RadioButton rdBtnAdditionNegative;
        private System.Windows.Forms.RadioButton rdBtnAdditionPositive;
    }
}

