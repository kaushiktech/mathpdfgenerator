﻿using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace MathGenerator
{
    public partial class MathGenForm : Form
    {
        public MathGenForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            combo_layout.SelectedIndex = 0;
            
                foreach (Control ctrl in GetAll(this, typeof(TextBox)))
                {
                    if (ctrl is TextBox)
                    {
                        TextBox tb = (TextBox)ctrl;
                        tb.TextChanged += new EventHandler(tb_TextChanged);
                    }
                }

            

        }
        private void tb_TextChanged(object sender, EventArgs e)
        { 
            try
            {
                if (int.Parse(((TextBox)sender).Text) < 0 || ((TextBox)sender).Text.StartsWith("-"))
                {
                    ((TextBox)sender).Text = "0"; //default value is 1, no action 
                }
            }
            catch(Exception tbE)
            {
                ((TextBox)sender).Text = "0";
            }
        }
        private void btn_generate_Click(object sender, EventArgs e)
        {



            var controls = GetAll(this, typeof(CheckBox));
            List<CheckBox> checkBoxes = new List<CheckBox>();
            foreach (Control c in controls)
            {
                checkBoxes.Add((CheckBox)c);
            }
            checkBoxes = checkBoxes.Where(chk => chk.Checked).ToList();
            if (checkBoxes.Count > 0 && int.Parse(txtQuestions.Text) >0)
            {
                List<String> functions = new List<String>();
                foreach (CheckBox checkBox in checkBoxes)
                {
                    functions.Add(checkBox.Tag.ToString());
                }
                MathGenerator math = new MathGenerator();
                List<MathOutput> outputList = math.GenerateMath(functions, Convert.ToInt16(txtQuestions.Text));
                string pdfPath = "questions.pdf";
                try
                {
                    using (FileStream msReport = new FileStream(pdfPath, FileMode.Create))
                    {
                        //step 1
                        using (Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 70f, 40f))
                        {
                            try
                            {
                                // step 2
                                PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, msReport);
                                pdfWriter.PageEvent = new ITextEvents();
                                //open the stream 
                                pdfDoc.Open();

                                PdfContentByte cb = pdfWriter.DirectContent;
                                PdfPTable table = new PdfPTable(Convert.ToInt16(combo_layout.SelectedItem));
                                table.DefaultCell.Border = 0;
                                table.DefaultCell.PaddingBottom = 10;
                                table.DefaultCell.PaddingTop = 10;
                                int count = 1;
                                foreach (MathOutput output in outputList)
                                {
                                    switch (output.MathType)
                                    {
                                        case MathType.Default:
                                        case MathType.Algebra:
                                        case MathType.Division:
                                            table.AddCell(count + ") " + output.OutputMath);
                                            break;
                                        case MathType.Multiplication:
                                            PdfPCell pCell = new PdfPCell();
                                            pCell.Border = 0;
                                            pCell.PaddingBottom = 10;
                                            pCell.PaddingTop = 0;
                                            var p = new Paragraph();
                                            int space = 0;

                                            p.Add(" " + (count + ") ") + output.OutputMath.Split('|')[0].PadLeft(15) + "\n");
                                            p.Add("       ×" + output.OutputMath.Split('|')[1].PadLeft(15) + "\n");
                                            p.Add("____________" + "\n");
                                            p.Alignment = Element.ALIGN_RIGHT;
                                            pCell.AddElement(p);

                                            table.AddCell(pCell);
                                            break;
                                        case MathType.Fraction:
                                            PdfPCell pfractionCell = new PdfPCell();
                                            pfractionCell.Border = 0;
                                            pfractionCell.PaddingBottom = 10;
                                            pfractionCell.PaddingTop = 0;
                                            var pfraction = new Paragraph();
                                            string op = "+";
                                            switch (output.FractionOperator)
                                            {
                                                case FractionOperator.Add:
                                                    op = "+";
                                                    break;
                                                case FractionOperator.Substract:
                                                    op = "-";
                                                    break;
                                                case FractionOperator.Multiplication:
                                                    op = "*";
                                                    break;
                                                case FractionOperator.Division:
                                                    op = "/";
                                                    break;
                                            }
                                            Chunk num1 = new Chunk(output.OutputFraction.Num1.ToString());
                                            Chunk num2 = new Chunk(output.OutputFraction.Num2.ToString());
                                            num1 = num1.SetUnderline(1f, -2f);
                                            num2 = num2.SetUnderline(1f, -2f);
                                            pfraction.Add(count.ToString() + ") \n");
                                            pfraction.Add(num1);
                                            pfraction.Add(" " + op + " ");
                                            pfraction.Add(num2);
                                            pfraction.Add(" = ? \n");
                                            pfraction.Add(output.OutputFraction.Denom1.ToString());
                                            pfraction.Add("    ");
                                            pfraction.Add(output.OutputFraction.Denom2.ToString());
                                            pfractionCell.AddElement(pfraction);
                                            table.AddCell(pfractionCell);
                                            break;
                                    }
                                    count++;
                                }
                                int cellToCompletion = count % Convert.ToInt16(txtQuestions.Text);
                                if (cellToCompletion != 0)
                                    for (int i = 0; i < cellToCompletion; i++)
                                    {
                                        table.AddCell(" ");
                                    }

                                pdfDoc.Add(table);
                                pdfDoc.Close();
                            }
                            catch (Exception ex)
                            {
                                //handle exception
                            }
                            finally
                            {
                                Process.Start("questions.pdf");
                            }
                        }
                    }
                }
                catch (IOException ioex)
                {
                    MessageBox.Show("Please close questions file or copy and open it from another locaiton");
                }
                pdfPath = "answers.pdf";
                try
                {
                    using (FileStream msReport = new FileStream(pdfPath, FileMode.Create))
                    {
                        //step 1
                        using (Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 70f, 40f))
                        {
                            try
                            {
                                // step 2
                                PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, msReport);
                                pdfWriter.PageEvent = new ITextEvents();
                                //open the stream 
                                pdfDoc.Open();

                                PdfContentByte cb = pdfWriter.DirectContent;
                                PdfPTable table = new PdfPTable(Convert.ToInt16(combo_layout.SelectedItem));
                                table.DefaultCell.Border = 0;
                                table.DefaultCell.PaddingBottom = 10;
                                table.DefaultCell.PaddingTop = 10;
                                int count = 1;
                                foreach (MathOutput output in outputList)
                                {
                                    switch (output.MathType)
                                    {
                                        case MathType.Default:
                                            table.AddCell(count + ") " + output.OutputMath + output.Answer);
                                            break;
                                        case MathType.Division:
                                            table.AddCell(count + ") " + output.Answer);
                                            break;
                                        case MathType.Algebra:
                                            table.AddCell(count + ") " + output.OutputMath + ", x = " + output.Answer);
                                            break;
                                        case MathType.Multiplication:
                                            PdfPCell pCell = new PdfPCell();
                                            pCell.Border = 0;
                                            pCell.PaddingBottom = 10;
                                            pCell.PaddingTop = 0;
                                            var p = new Paragraph();
                                            int space = 0;

                                            p.Add(" " + (count + ") ") + output.OutputMath.Split('|')[0].PadLeft(15) + "\n");
                                            p.Add("       ×" + output.OutputMath.Split('|')[1].PadLeft(15) + "\n");
                                            p.Add("____________" + "\n");
                                            p.Alignment = Element.ALIGN_RIGHT;
                                            p.Add(output.Answer.PadLeft(18));
                                            pCell.AddElement(p);
                                            table.AddCell(pCell);
                                            break;
                                        case MathType.Fraction:
                                            PdfPCell pfractionCell = new PdfPCell();
                                            pfractionCell.Border = 0;
                                            pfractionCell.PaddingBottom = 10;
                                            pfractionCell.PaddingTop = 0;
                                            var pfraction = new Paragraph();
                                            string op = "+";
                                            switch (output.FractionOperator)
                                            {
                                                case FractionOperator.Add:
                                                    op = "+";
                                                    break;
                                                case FractionOperator.Substract:
                                                    op = "-";
                                                    break;
                                                case FractionOperator.Multiplication:
                                                    op = "×";
                                                    break;
                                                case FractionOperator.Division:
                                                    op = "÷";
                                                    break;
                                            }
                                            Chunk num1 = new Chunk(output.OutputFraction.Num1.ToString());
                                            Chunk num2 = new Chunk(output.OutputFraction.Num2.ToString());
                                            Chunk num3 = new Chunk(output.AnswerFraction.Num1.ToString());
                                            num1 = num1.SetUnderline(1f, -2f);
                                            num2 = num2.SetUnderline(1f, -2f);
                                            num3 = num3.SetUnderline(1f, -2f);
                                            pfraction.Add(count.ToString() + ") \n");
                                            pfraction.Add(num1);
                                            pfraction.Add(" " + op + " ");
                                            pfraction.Add(num2);
                                            pfraction.Add(" = ");
                                            pfraction.Add(num3);
                                            pfraction.Add("\n");
                                            pfraction.Add(output.OutputFraction.Denom1.ToString());
                                            pfraction.Add("    ");
                                            pfraction.Add(output.OutputFraction.Denom2.ToString());
                                            pfraction.Add("    ");
                                            int padding = 0;

                                            padding = Math.Abs(output.AnswerFraction.Num1.ToString().Length - output.AnswerFraction.Denom1.ToString().Length) * 2;

                                            pfraction.Add(output.AnswerFraction.Denom1.ToString().PadLeft(padding));

                                            pfractionCell.AddElement(pfraction);
                                            table.AddCell(pfractionCell);
                                            break;
                                    }
                                    count++;
                                }
                                int cellToCompletion = count % Convert.ToInt16(txtQuestions.Text);
                                if (cellToCompletion != 0)
                                    for (int i = 0; i < cellToCompletion; i++)
                                    {
                                        table.AddCell(" ");
                                    }
                                pdfDoc.Add(table);
                                pdfDoc.Close();
                            }
                            catch (Exception ex)
                            {
                                //handle exception
                            }
                            finally
                            {
                                Process.Start("answers.pdf");
                            }

                        }
                    }
                }
                catch (IOException ioex)
                {
                    MessageBox.Show("Please close answers file or copy and open it from another locaiton");
                }
            }
            else
            {
                MessageBox.Show("Please select an option and enter a valid value for number of questions");
            }
        }

        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
    }
}

